# Creating an anonymous linked-in account

There are many reasons why you would want to create an anonymous linked in account. In the course of an investigation you may want to anonymously browse company personnel you're investigation.

Let's try the following steps:
* Create anonymous email
* Create anonymous Linked In account with that email

## Create anonymous email

In order to even start the process I logged in through Tor Browser. I do not want my activity to be tracked. I also want to make sure I try out everything anonymously to start with. I also documented my process offline rather than using an online text cloud tool.

 To create an anonymous email I used the service by Proton Mail. It does not ask for a phone number or another email. The backup email is for recovery. Do not enter that and make sure you always access the email using Tor. Also document your username and password using keepass so as to be able to retrieve them.

Using Tor, I was able to create an email on [YandexMail.com](YandexMail.com)

List to verify:
http://thesimplecomputer.info/free-webmail-for-better-privacy

  u:linkmein@yandex.com
  p:NoPasswordLasts4Ever25

TextNow: u:linkmein1 p:NoPasswordLasts4
Phone number:

Linked-In : Linkmein p: NoPasswordLasts
